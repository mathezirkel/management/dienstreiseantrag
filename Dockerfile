FROM python:3.12.1

ENV INPUT_DATA="dienstreise.json"
ENV INPUT_FORM="dienstreise.pdf"
ENV FINAL="0"

RUN apt-get update && \
    apt-get install -y --no-install-recommends pdftk && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN mkdir -p /app/input && \
    mkdir -p /app/output && \
    mkdir -p /app/fdf && \
    mkdir -p /app/tmp/

RUN useradd -M appuser && chown -R appuser:appuser /app

COPY ./src/ /app/src/

USER appuser

WORKDIR /app

CMD ["/bin/sh", "./src/main.sh"]
