from datetime import datetime, timedelta, time
from enum import Enum
from os import getenv
from pathlib import Path
import json
from typing import Any

IBAN_MATHEZIRKEL = "DE97 7205 0000 0251 0746 96"
BIC_MATHEZIRKEL = "AUGSDE77XXX"
NAME_OF_BANK_MATHEZIRKEL = "Stadtsparkasse Augsburg"
ADDRESS_BRUDER_KLAUS_HEIM = """Bruder-Klaus-Heim
St. Michael Straße 15
86450 Altenmünster"""

# Costs for coffee breaks per day and person
COFFEE_BREAK_COSTS = 3.0

def get_date_str(timestamp: str, offset: int = 0) -> str:
    d = datetime.strptime(timestamp, '%Y-%m-%d %H:%M:%S').date()
    d = d + timedelta(days=offset)
    return d.strftime('%d.%m.%Y')

def get_time_str(timestamp: str) -> str:
    t = datetime.strptime(timestamp, '%Y-%m-%d %H:%M:%S').time()
    return t.strftime('%H:%M')

def get_eur(euro: int | float) -> str:
    return f"{euro:.2f}".replace('.', ',')

def get_amount_of_coffee_break_participations(
    coffee_breaks: list[str],
    arrival: str,
    departure: str,
) -> int:
    arrival_date = datetime.strptime(arrival, '%Y-%m-%d %H:%M:%S')
    departure_date = datetime.strptime(departure, '%Y-%m-%d %H:%M:%S')
    coffee_break_dates = [datetime.strptime(date, '%Y-%m-%d') for date in coffee_breaks]
    return sum(arrival_date <= date <= departure_date for date in coffee_break_dates)

def get_hotel_expenses(
    accommodation_costs: float,
    coffee_breaks: list[str],
    arrival: str,
    departure: str,
) -> str:
    amount_of_coffee_break_participations = get_amount_of_coffee_break_participations(coffee_breaks, arrival, departure)
    return get_eur(accommodation_costs + amount_of_coffee_break_participations * COFFEE_BREAK_COSTS)

def get_accommodation_statement(
    accommodation_costs: float,
    overnight_costs: float,
    coffee_breaks: list[str],
    arrival: str,
    departure: str,
) -> str:
    # Floor division
    amount_of_regular_nights = int(accommodation_costs / overnight_costs)
    costs_for_irregular_night = accommodation_costs - amount_of_regular_nights * overnight_costs
    amount_of_coffee_break_participations = get_amount_of_coffee_break_participations(coffee_breaks, arrival, departure)
    day_label = "Tag" if amount_of_coffee_break_participations == 1 else "Tagen"
    regular_night_statement = f"{amount_of_regular_nights} Nächte à {get_eur(overnight_costs)} Euro inkl. Vollpension"
    irregular_night_statement = \
        f"1 Nacht à {get_eur(costs_for_irregular_night)} Euro inkl. Vollpension" \
            if costs_for_irregular_night \
            else ""
    coffee_break_statement = \
        f"separate Berechnung {get_eur(COFFEE_BREAK_COSTS)} pro Tag an {amount_of_coffee_break_participations} {day_label} für Nachmittagsgebäck" \
        if amount_of_coffee_break_participations \
        else ""

    return "Übernachtungskosten: " + " + ".join(
        statement for statement in [regular_night_statement, irregular_night_statement, coffee_break_statement]
        if statement
    )

class Meal(Enum):
    BREAKFAST = 0
    LUNCH = 1
    DINNER = 2

def get_meal_date(timestamp: str, is_arrival: bool, meal: Meal) -> str:
    def calculate_offset(t: time, thresholds: tuple[time,time], is_arrival: bool) -> int:
        arrival_cutoff, departure_cutoff = thresholds
        if is_arrival:
            if t >= arrival_cutoff:
                return 1
        else:
            if t <= departure_cutoff:
                return -1
        return 0

    t = datetime.strptime(timestamp, '%Y-%m-%d %H:%M:%S').time()

    # Meal times as tuples:
    # 1: This is the time by which a person must arrive to qualify for the meal on the arrival day.
    # 2: This is the time by which a person must be present to qualify for the meal on the departure day.
    meal_times = {
        Meal.BREAKFAST: (time(8, 0), time(9, 0)),
        Meal.LUNCH: (time(12, 0), time(13, 0)),
        Meal.DINNER: (time(18, 0), time(19, 0)),
    }

    thresholds = meal_times[meal]
    offset = calculate_offset(t, thresholds, is_arrival)
    return get_date_str(timestamp, offset)

def get_signing_date(departure: str) -> str:
    if int(getenv("FINAL")):
        return get_date_str(departure)
    return datetime.now().strftime('%d.%m.%Y')

def make_form_data(
    instructor: dict[str, Any],
    event_data: dict[str, Any],
    coffee_breaks: list[str]
) -> list[dict[str, str]]:
    def get_additional_costs_section() -> dict[str,str]:
        if additional_costs := event_data['additionalCosts']:
            return {
                "Sonstige Kosten Begründung": "On",
                "EUR Sonstige Nebenkosten": f"{get_eur(additional_costs)}",
                "Begründung Nebenkosten": event_data.get('reasonOfAdditionalCosts', ""),
            }
        return {}

    instructor_form_data = {
        "Genehmigung": "Dienstreise",
        "im Inland/Ausland": "Inland",

        # Section 1
        "1 Angaben zumr Reisenden": (
            f"{instructor['instructor']['familyName']}, "
            f"{instructor['instructor']['givenName']}"
        ),
        "PersonalNr LfF": instructor['personnelNumber'] or "",
        "vollständige Anschrift 1": (
            f"{instructor['instructor']['street']} "
            f"{instructor['instructor']['streetNumber']}, "
            f"{instructor['instructor']['postalCode']} "
            f"{instructor['instructor']['city']}"
        ),
        "E-Mail": instructor['emailWork'],
        "Telefon dienstlich 1": instructor['telephoneWork'] or "",
        "Fakultät, Lehrstuhl": f"{instructor['faculty']}, {instructor['chair']}",
        "Funktion Mitarbeiter*in": instructor['jobTitle'],
        "IBAN": IBAN_MATHEZIRKEL,
        "BIC": BIC_MATHEZIRKEL,
        "Name der Bank": NAME_OF_BANK_MATHEZIRKEL,

        # Section 2
        "Datum Reisebeginn": get_date_str(instructor['startOfTravel']) or "",
        "Uhrzeit Hinreise": get_time_str(instructor['startOfTravel']) or "",
        "Reisebeginn wo": "Dienststelle",
        "Datum Ankunft Geschäftsort": get_date_str(instructor['arrivalAtBusinessLocation']) or "",
        "Uhrzeit Ankunft Geschäftsort": get_time_str(instructor['arrivalAtBusinessLocation']) or "",
        "Beginn Dienstgeschäft Datum": get_date_str(instructor['startOfBusinessActivities']) or "",
        "Beginn Dienstgeschäft Uhrzeit": get_time_str(instructor['startOfBusinessActivities']) or "",
        "Ende Dienstgeschäft Datum": get_date_str(instructor['endOfBusinessActivities']) or "",
        "Ende Dienstgeschäft Uhrzeit": get_time_str(instructor['endOfBusinessActivities']) or "",
        "Datum Rückreise": get_date_str(instructor['startOfReturnJourney']) or "",
        "Uhrzeit Rückreise": get_time_str(instructor['startOfReturnJourney']) or "",
        "Ende Reise Datum": get_date_str(instructor['endOfTravel']) or "",
        "Ende Reise Uhrzeit": get_time_str(instructor['endOfTravel']) or "",
        "Reiseende wo": "Dienststelle",
        "Reisezweck": event_data["purposeOfBusinessTrip"],
        "Adresse Geschäftsort": ADDRESS_BRUDER_KLAUS_HEIM,
        "Group5": "Rückkehr möglich nein",

        # Section 3

        # Section 4
        "Antrag Tagegeld": "On",
        "Group8": "Tagegeld nein",
        "Hotel oä": "On",
        "EUR Hotelkosten": get_hotel_expenses(
            instructor["accommodationCosts"],
            coffee_breaks,
            instructor['arrivalAtBusinessLocation'],
            instructor['startOfReturnJourney'],
        ),
        "Verpflegung in Übernachtungskosten": "On",
        "Frühstück_Übernachtung": "On",
        "Verpflegung Übernachtung": "ja",
        "Datum_von_Übernachtung_Frühstück": get_meal_date(
            timestamp=instructor['arrivalAtBusinessLocation'],
            is_arrival=True,
            meal=Meal.BREAKFAST
        ),
        "Datum_bis_Übernachtung_Frühstück": get_meal_date(
            timestamp=instructor['startOfReturnJourney'],
            is_arrival=False,
            meal=Meal.BREAKFAST
        ),
        "Mittag_Übernachtung": "On",
        "Verpflegung Übernachtung Mittag": "Ja",
        "Datum_von_Übernachtung_Mittag": get_meal_date(
            timestamp=instructor['arrivalAtBusinessLocation'],
            is_arrival=True,
            meal=Meal.LUNCH
        ),
        "Datum_bis_Übernachtung_Mittag": get_meal_date(
            timestamp=instructor['startOfReturnJourney'],
            is_arrival=False,
            meal=Meal.LUNCH
        ),
        "Abend_Übernachtung": "On",
        "Verpflegung Übernachtung Abend": "Ja",
        "Datum_von_Übernachtung_Abend": get_meal_date(
            timestamp=instructor['arrivalAtBusinessLocation'],
            is_arrival=True,
            meal=Meal.DINNER
        ),
        "Datum_bis_Übernachtung_Abend": get_meal_date(
            timestamp=instructor['startOfReturnJourney'],
            is_arrival=False,
            meal=Meal.DINNER
        ),
        "Begründung Verpflegung": get_accommodation_statement(
            instructor["accommodationCosts"],
            event_data["overnightCosts"],
            coffee_breaks,
            instructor['arrivalAtBusinessLocation'],
            instructor['startOfReturnJourney'],
        ),

        # Section 5
        **get_additional_costs_section(),

        # Section 6
        "Group18": "Abschlag nein",
        "Grundlage": "Ja",
        "Richtigkeit": "Ja",

        # Section 7
        "Verrechnung mit": "On",
        "Kapitel": event_data['budgetSection'] or "",
        "Titel": event_data['budgetItem'] or "",
        "Kostenstelle Haushalt": event_data['costCenterBudget'] or "",
        "Kostenart": event_data['costType'] or "",
        "Kostenstelle KLR": event_data['costCenterCostAndActivityAccounting'] or "",
        "Kostenträger T": event_data['costObject'] or "",

        # Signing Field
        "Ort Datum": f"Augsburg, {get_signing_date(instructor['endOfTravel'])}",
        "Name Vorgesetzter": instructor['contractWith']
    }
    return instructor_form_data


def fdf_str_field(key: str, value: str) -> str:
    return f"<field name=\"{key}\"><value>{value}</value></field>"


def make_fdf_str(form_data: dict[str, str]) -> str:
    form_str = '\n'.join([fdf_str_field(k, v) for k, v in form_data.items()])
    fdf_str = f"""<?xml version="1.0" encoding="UTF-8"?>
<xfdf xmlns="http://ns.adobe.com/xfdf/" xml:space="preserve">
<fields>
{form_str}
</fields>
</xfdf>"""
    return fdf_str


def main() -> None:
    data_path = Path(f"./input/{getenv('INPUT_DATA')}")
    coffee_breaks = json.loads(getenv('COFFEE_BREAKS'))

    with open(data_path, 'r', encoding="utf-8") as input_file:
        event_data = json.load(input_file)

    instructors = event_data.pop("instructorExtensions")
    for instructor in instructors:
        if instructor['hasContract']:
            instructor_form_data = make_form_data(instructor, event_data, coffee_breaks)
            fdf_str = make_fdf_str(instructor_form_data)
            family_name = instructor['instructor']['familyName']
            given_name = instructor['instructor']['givenName']
            filename = f"{family_name}_{given_name}.xfdf"
            with open(Path(f"./fdf/{filename}"), 'w', encoding='utf-8') as fdf_file:
                fdf_file.write(fdf_str)


if __name__ == "__main__":
    main()
