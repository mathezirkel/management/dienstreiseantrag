#!/bin/sh

set -e

python3 ./src/make_fdf.py

for file in ./fdf/*; do
    filename=$(basename "$file")
    base="${filename%.*}"
    if [ -f "./output/${base}.pdf" ]; then
        cp "./output/${base}.pdf" "./tmp/${base}.pdf"
        input_file=""./tmp/${base}.pdf""
    else
        input_file="./input/${INPUT_FORM}"
    fi
    pdftk "$input_file" fill_form "$file" output "./output/${base}.pdf"
done
