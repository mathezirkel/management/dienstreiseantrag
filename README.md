# Dienstreiseantrag

## Beschreibung

Das Skript füllt automatisch die notwendigen Dienstreiseanträge für das Mathecamp aus.

## Anleitung

### Dienstreiseanträge Vorlage

Die Vorlage für den Dienstreiseantrag befindet sich im Intranet.
Damit das Skript funktioniert, muss die Datei mit dem folgenden Befehl angepasst werden:

```commandline
qpdf --decrypt "210301_dienstreise-formular_antrag-abrechnung.pdf" "dienstreise.pdf"
```

### Dienstreiseanträge erstellen

```shell
docker run \
--rm \
-e FINAL="0" \
-e COFFEE_BREAKS="[\"2024-08-19\",\"2024-08-23\"]"
-v "path/to/input/dienstreise.json":/app/input/dienstreise.json \
-v "path/to/dienstreise.pdf":/app/input/dienstreise.pdf \
-v "path/to/output/":/app/output/ \
registry.gitlab.com/mathezirkel/management/dienstreiseantrag:latest
```

Wenn für eine Person noch kein Dienstreiseantrag vorhanden ist, wird dieser auf Grundlage der
Vorlage (siehe Dokumentation) erstellt. Andernfalls wird der vorhandene Dienstreiseantrag
bearbeitet. Dabei bleiben Felder, die nicht vom Skript gefüllt werden, unverändert.
Insbesondere wird Abschnitt 3 (Fahrtkosten) aktuell nicht vom Skript befüllt.

## Dokumentation

### Notwendige Dateien

Die folgenden Dateien müssen in den Ordner `/app/input/` gemounted werden.

* `dienstreise.json`
  Die Datei enthält die Daten, die in das Formular eingetragen werden sollen,
  siehe [Schema](./docs/dienstreise.schema.json), und wird automatisch aus der Datenbank erstellt.

* `dienstreise.pdf`
  Das Formular, in das die Daten eingetragen werden sollen.
  In [data_fields.txt](./docs/data_fields.txt) sind die enthaltenen Formularfelder dokumentiert.
  Die Datei wurde automatisch mit dem Befehl

  ```shell
  pdftk dienstreise.pdf dump_data_fields > ./docs/data_fields.txt
  ```

  generiert.

### Konfiguration

#### Umgebungsvariablen

| Umgebungsvariable | Beschreibung | Typ | default |
|-------------------|--------------|-----|---------|
| `INPUT_DATA`      | Dateiname der JSON-Datendatei | `string` | `dienstreise.json` |
| `INPUT_FORM`      | Dateiname der PDF-Formularvorlage | `string` | `dienstreise.pdf` |
| `FINAL`           | `0`: Heutiges Datum <br>`1`: Datum des letztes Dienstreisetages | `int` | 0 |
| `COFFEE_BREAKS`   | Liste an Tagen mit Kaffee & Kuchen | `string` | |

#### Mounts

| Mount Pfad | Beschreibung |
|------------|--------------|
| `/app/input/` | Ordner für die Quelldateien |
| `/app/output/` | Ordner für die Ausgabedateien |
| `/app/fdf/` | Ordner für XFDF-Dateien (nur für debug) |

## Release

Um eine Version zu veröffentlichen, pushe einen Git tag. Der neueste Tag wird automatisch mit dem
Tag `latest` versehen.

## License

Das Repository ist unter [GPL-3.0](https://www.gnu.org/licenses/gpl-3.0.txt) lizenziert,
siehe [LICENSE](.LICENSE).
